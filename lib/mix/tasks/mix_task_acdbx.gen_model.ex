defmodule Mix.Tasks.Acdbx.GenModel do
  use Mix.Task

  @shortdoc "Generate a model from a database table - params config, model [, table, output]"

  def run([db_cfg, model]), do: run([db_cfg, model, pluralize(model), nil])
  def run([db_cfg, model, table]), do: run([db_cfg, model, table, nil])
  def run([db_cfg, model, table, output]) do
    path = output || "./#{model}.ex"
    content = get_table_desc(db_cfg, model, table)
    File.write(path, content)
  end

  defp get_table_desc(cfg, model, table) do
    {:ok, conn} = Acdbx.MysqlConn.init(config: Application.get_env(:acdbx, :config)[String.to_atom(cfg)])
    {:ok, _names, desc} = :mysql.query(conn, "describe #{table}")
    result= desc |> Enum.map(fn([col, type | _]) -> get_size_type(String.to_atom(col), type) end)
    case length(String.split(model,".")) < 3 do
      true -> raise "Models names must have at minimum 'Project.NameSpace.ModelName' format"
      false -> nil
    end
    [
      "defmodule #{model} do",
      "  use Acdbx.Model\n",
      "  table :#{table}\n",
      "  fields [",
      Enum.map(result, &("    #{inspect &1}")) |> Enum.join(",\n"),
      "  ]\n",
      "#  relationships [",
      "#    name: {<|:one_to_one | :one_to_many |>, :src_id, fully_qualified_model_name, :assoc_id}",
      "#    eg: address: {:one_to_one, :address_id, Example.Models.Address, :id}",
      "#  ]",
      "end\n"
    ] |> Enum.join("\n")
  end

  defp get_size_type(name, type) do
    case Regex.run(~r/(int|bigint|varchar|float|decimal|tinyint)\(([0-9]+,?[0-9]*)\)/, type) do
      [_,"int", _s] -> {name, :int}
      [_,"bigint", _s] -> {name, :int}
      [_,"varchar", s] -> {name, :str, String.to_integer(s)}
      [_,"float", s] -> {name, :float, String.replace(s,",",".") |> String.to_float}
      [_,"decimal", _s] -> {name, :money}
      [_,"tinyint", s] -> {name, :int, String.to_integer(s)}
      _ -> get_other_types(name, type)
    end
  end
  defp get_other_types(name, type) do
    case type do
      "datetime" -> {name, :date_time}
      "date" -> {name, :date}
      "timestamp" -> {name, :date_time}
       t when t == "text" or t == "longtext" -> {name, :str}
      _ -> {name, :unknown}
    end
  end

  defp pluralize(model) do
    case Regex.run(~r/([a-z_]+)([a-z]{1])$/, model) do
      [name, _, "ss"] -> name <> "es"
      [name, _, _] -> name <> "s"
      _ -> model
    end
  end

end
