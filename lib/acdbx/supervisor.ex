defmodule Acdbx.Supervisor do
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, [], [])
  end

  def init([]) do
     #children = [
      #get_child_spec(:mysql_ceasy_pool, Czwebex.Db.Ceasy, 2, 10)
    #]

    Enum.map(Application.get_env(:acdbx, :config), &get_child_spec(&1))
    |> supervise(strategy: :one_for_one)
  end

  def get_pool_opts(pool_name, module, start, max) do
    [
      {:name, {:local, pool_name}},
      {:worker_module, module},
      {:size, start},
      {:max_overflow, max}
    ]
  end

  def get_module(:mysql), do: Acdbx.MysqlConn

  def get_child_spec({pool_name, opts}) do
    max_conn = opts[:max_connections] || 10
    min_conn = max(1, div(max_conn, 2))

    pool_options = get_pool_opts(pool_name, get_module(opts[:type]), min_conn, max_conn)
    :poolboy.child_spec(pool_name, pool_options, [config: opts])
  end

end
