defmodule Acdbx.Repo do
  defmacro __using__(options) do
    quote do
      require Logger
      defp db_cfg, do: Application.get_env(:acdbx, :config)[unquote options[:conn]]
      defp query_timeout, do: db_cfg[:query_timeout] || 3000
      defp conn_module do
        case db_cfg[:type] do
          :custom -> db_cfg[:conn_module]
          :mysql -> Acdbx.MysqlConn
          _ -> nil
        end
      end
      defp conn_pool, do: unquote options[:conn]

      def get_server_time_zone(), do: conn_module.get_server_time_zone
      def get_db_time_zone(), do: db_cfg[:db_tz] || get_server_time_zone()
      # transform_in - Data into the app from database
      defp transform_in(dt={{_y,_m,_d},{_h, _i, _s}}), do: transform_in(:date_time, dt)
      defp transform_in(val), do: val
      defp transform_in(:bool, _, 0), do: false
      defp transform_in(:bool, _, 1), do: true
      defp transform_in(:date_time, _, dt={{y,m,d},{_,_,_}}) when y == 0 or m == 0 or d == 0, do: nil
      defp transform_in(:date_time, _, dt={{_y,_m,_d},{_h, _i, _s}}) do
        case get_server_time_zone() != get_db_time_zone do
          true -> Timex.to_datetime(dt, get_db_time_zone)
                  |> Timex.Timezone.convert(get_server_time_zone())
          false -> Timex.to_datetime(dt, get_db_time_zone)
        end
      end
      defp transform_in(:date_time, a, dt={y,m,d}), do: transform_in(:date_time, a, {dt, {0,0,0}})
      defp transform_in(:date, _, dt={y,m,d}) when y == 0 or m == 0 or d == 0, do: nil
      defp transform_in(:date, _, dt={_y,_m,_d}) do
        transform_in(:date_time, nil, {dt, {0,0,0}})
      end
      defp transform_in(:str, _, :null), do: nil
      defp transform_in(:int, _, :null), do: 0
      defp transform_in(:money, _, :null), do: 0.0
      defp transform_in(:money, _, value), do: Decimal.new(value, 2)
      defp transform_in(_, _, :null), do: nil
      defp transform_in(_, _, data), do: data

      def transform_in(field_spec, data) do
        case field_spec do
          [_name, type, size | _] -> transform_in(type, size, data)
          [_name, type | _] -> transform_in(type, nil, data)
          _ -> data
        end
      end
      defoverridable [transform_in: 3]

      # transform_out - Data out of the app to database
      defp transform_out(module, k, v) when is_atom(k) do
        transform_out(module.field_spec(k), v)
      end
      defp transform_out([_, :int], data) when is_binary(data) do
        String.trim(data) |> String.to_integer
      end
      defp transform_out([_, :bool], "true"), do: true
      defp transform_out([_, :bool], "false"), do: false
      defp transform_out(d = {y,m,d}), do: Timex.to_datetime(d, get_db_time_zone)
      defp transform_out([_, :money], value), do: Decimal.as_string(value)
      defp transform_out(field_spec, data) do
        #IO.puts "#{inspect field_spec}, #{inspect data}"
        data
      end


      defp build_record(nil, module, _), do: nil
      defp build_record(r, module, keys) when is_map(r) do
        Enum.reduce(keys, module.get_model, fn(k, acc) ->
                    Map.put(acc, k, transform_in(module.field_spec(k), r[k])) end)
      end

      defp build_records(nil, _, _), do: []
      defp build_records(recs, module, keys) do
        case is_list(recs) do
          true -> recs |> Enum.map(fn(r) -> build_record(r, module, keys) end)
          false -> build_record(recs, module, keys)
        end
      end

      # defp get_fields(model), do: Map.keys(model) |> Enum.drop(1)

      @shortdoc "if ignore_auto_fields is set in a module, created_at, updated_at is ignored"
      defp update_auto_fields(true, keys_vals, _), do: keys_vals
      defp update_auto_fields(false, {keys, vals}, {add_keys, add_vals}) do
        {keys ++ add_keys, vals ++ add_vals}
      end

      defp get_head(nil), do: nil
      defp get_head([]), do: nil
      defp get_head([x|_]), do: x

      defp extract_relationship(module, relationship) do
        case module.relationship(relationship) do
          assoc = {_rel_type, _src_id, _module, _assoc_id, opts} -> assoc
                  {rel_type, src_id, rel_module, assoc_id} ->{rel_type, src_id, rel_module, assoc_id, []}
                  {rel_type, src_id, rel_module} -> {rel_type, src_id, rel_module, :id, []}
        end
      end

      defp get_fields_with(model, extra_fields) when is_map(model) do
        Enum.uniq((model.__struct__.keys |> tl) ++ extra_fields)
      end
      defp get_fields_with(module, extra_fields) do
        Enum.uniq((module.keys |> tl) ++ extra_fields)
      end

      defp get_relationship_src_fields(module, relationships) do
        Enum.uniq(module.keys ++
            Enum.map(relationships, fn(r) ->
                {_, src_id,_,_,_} = extract_relationship(module, r)
                src_id
            end)
        )
      end

      @shortdoc "Gets records array with max_rows for a given id, eg: %{records: [], max_rows: <nn>}"
      defp get(module, opts) when is_list(opts) do
        table = module.table
        opt_qtimeout = opts[:timeout] || query_timeout
        new_opts = case opts[:select] do
          nil -> Keyword.put(opts, :select, module.keys)
          _   -> opts
        end
        |> Keyword.put(:timeout, opt_qtimeout)
        {result, max_rows} = conn_module.from(conn_pool, table, new_opts)
        %{records: result |> Enum.map(fn(r) -> build_record(r, module, new_opts[:select]) end),
         max_rows: max_rows}
      end
      @depreciated "Use get_record(module, id)"
      defp get(module, id) when is_number(id) do
        get(module, where: [id: id])
      end
      @depreciated "Use get_record(module, id)"
      defp get_one(module, id) when is_number(id) do
        get(module, where: [id: id]) |> Map.get(:records) |> get_head
      end
      @depreciated "Use get_records(module, opts)"
      defp get_one(module, opts), do: get_record(module, opts)

      defp get_record(module, opts) when is_list(opts) do
        get(module, opts) |> Map.get(:records) |> get_head
      end
      @shortdoc "Gets records array with max_rows for a given id, eg: %{records: [], page: {n, n}}"
      defp get_records(module, opts) when is_list(opts) do
        get(module,opts)
        |> Map.get(:records)
      end
      defp get_record(module, id) when is_number(id) do
        get(module, where: [id: id])
        |> Map.get(:records)
        |> hd
      end
      @shortdoc "Just gets the records array"
      defp get_records(module, opts) when is_map(opts) do
        get(module, opts)
        |> Map.get(:records)
      end
      defp get_records_with(module, relationships, opts \\ []) do
        get_with(module, relationships, opts)
        |> Map.get(:records)
      end
      defp get_with(module, relationships, opts \\ []) when is_list(relationships) do
        fields = get_relationship_src_fields(module, relationships)
        upd_fields = case opts[:select] do
          nil -> fields
          sfields ->  Enum.uniq(sfields ++ fields)
        end
        opt_qtimeout = opts[:timeout] || query_timeout
        upd_opts = Keyword.put(opts, :select, upd_fields) |> Keyword.put(:timeout, opt_qtimeout)
        result = get(module, upd_opts)
        selected_recs = case length(result.records) > 1000 do
          true -> raise "Too many records to upload relationships.  Adjust query to limit to 1000 or less or iterate and get via get_rel(..)"
          false -> result.records
        end
        recs = Enum.reduce(relationships, selected_recs, fn(rel, srecs) ->
          get_rel(srecs, rel)
        end)
       %{records: recs, max_rows: result.max_rows}
      end
      defp get_one_with(module, id, relationships) when is_list(relationships) do
        get_with(module, relationships, where: [id: id])
        |> Map.get(:records)
        |> get_head
      end

      defp get_rel([], _, _), do: []
      defp get_rel(records, relationship), do: get_rel(records, relationship, nil)
      defp get_rel(records, relationship, p_opts) when is_list(records) and is_atom(relationship) do
        module = (records |> hd).__struct__
        {rel_type, src_id, rel_module, assoc_id, r_opts} = extract_relationship(module, relationship)

        opts = p_opts || r_opts
        rel_fields = opts[:select] || rel_module.keys
        upd_fields = [assoc_id | rel_fields] |> Enum.uniq #assoc_id is necessary for binding the record

        src_ids = Enum.map(records, fn(r) -> Map.get(r,src_id) end) |> Enum.uniq |> Enum.join(",")
        where =
          case opts[:where] do
            nil -> Keyword.put([], assoc_id, in: src_ids)
            _ -> Keyword.put(opts[:where], assoc_id, [in: src_ids])
          end

        upd_opts = Keyword.merge([select: upd_fields, where: where], opts || [])

        {tmp_assoc_recs, _max_rows} = conn_module.from(conn_pool, rel_module.table, upd_opts)
        assoc_recs = tmp_assoc_recs |> Enum.group_by(fn(r) -> Map.get(r, assoc_id) end) |> Map.new
        Enum.map(records, fn(record) ->
          case rel_type do
            :one_to_one ->
                Map.put(record, relationship, Map.get(assoc_recs, Map.get(record, src_id))
                        |> get_head |> build_record(rel_module, rel_fields))
            _ ->
                Map.put(record, relationship, Map.get(assoc_recs, Map.get(record, src_id))
                        |> build_records(rel_module, rel_fields))
          end
        end)
      end

      defp get_joined({m1, m2}, {fld1, fld2}, opts) do
        e_opts = Keyword.put(opts, :on, {fld1, fld2})
        {results, max_rows} =
          conn_module.from(conn_pool, {m1.table, m2.table}, e_opts)

          %{records: results, max_rows: max_rows}
      end

      @shortdoc "raw_query(sql, vals) - executes a given select query and returns an array[map] of data"
      defp raw_query(sql, vals) do
        {records, _max_count} = conn_module.raw_query(conn_pool, sql, vals, query_timeout)
        records |> Enum.map(fn(row) ->
          Enum.map(row, fn({k,v}) -> {k, transform_in(v)} end) |> Map.new
        end)
      end

      defp raw_update(sql, vals) do
        conn_module.raw_update(conn_pool, sql, vals)
      end

      defp prep_create(record) do
        module = record.__struct__
        upd_record = Map.drop(record, [:created_at, :updated_at])
        keys = module.keys
        {keys, vals} = Enum.reduce(keys, {[],[]}, fn(k, {k_arr, v_arr}) ->
          case Map.get(upd_record, k) do
            nil -> {k_arr, v_arr}
            val -> {[k | k_arr], [transform_out(module, k, val) | v_arr]}
          end
        end)
        update_auto_fields(module.ignore_auto_fields, {keys,vals},
                            {[:created_at, :updated_at],
                             [conn_module.get_date_time_now, conn_module.get_date_time_now]})
      end

      # when you have array of values, eg: csv upload.
      defp create_record(module, vals) when is_list(vals) and is_atom(module) do
        keys = module.keys
        case conn_module.insert(conn_pool, module.table, keys, vals) do
          {:ok, _, [id | _]} -> {:ok, id}
            err             -> err
        end
      end
      defp create_record(record, upsert: true) do
        module = record.__struct__
        {upd_keys, upd_vals} = prep_create(record)
        case conn_module.upsert(conn_pool, module.table, upd_keys, upd_vals) do
          {:ok, _, [id | _]} -> {:ok, id}
            err             -> err
        end
      end
      defp create_record(record) do
        module = record.__struct__
        {upd_keys, upd_vals} = prep_create(record)
        case conn_module.insert(conn_pool, module.table, upd_keys, upd_vals) do
          {:ok, _, [id | _]} -> {:ok, id}
            err             -> err
        end
      end
      # Transaction version
      defp create_record(record, conn) when is_pid(conn) do
        module = record.__struct__
        {upd_keys, upd_vals} = prep_create(record)
        case conn_module.insert_trx(conn, module.table, upd_keys, upd_vals) do
          {:ok, _, [id | _]} -> {:ok, id}
            err             -> err
        end
      end

      # inserts using a single statement, efficient
      # use this when you don't need last_insert_id of all the records
      # MYSQL will only return the insert_id of the first insert
      defp create_records_batched(records) when is_list(records) do
        record = records |> hd
        module = record.__struct__
        insert_list = Enum.map(records, &prep_create/1)
        conn_module.bulk_insert(conn_pool, module.table, insert_list)
      end

      # This is a convenient function that woul loop through and insert
      # records and then return an array of last_insert_ids
      # returns {{ok_count, ids=[]}, {err_count, msgs=[]}}
      defp create_records(records) when is_list(records) do
        Enum.reduce(records, {{0, []},{0, []}}, fn(r, {k={ok_count, ids}, e={err_count, msgs}}) ->
          case create_record(r) do
            {:error, error} -> {k, {err_count + 1, [error | msgs]}}
            {:ok, c, [id|_]} -> {{ok_count + 1, [id | ids]}, e}
          end
        end)
      end


      defp make_sure_int(s) when is_integer(s), do: s
      defp make_sure_int(s), do: String.trim(s) |> String.to_integer

      defp prep_update(record) do
        module = record.__struct__
        keys = module.keys
        upd_record = Map.delete(record, :updated_at)
        {keys, vals} = Enum.reduce(keys, {[],[]}, fn(k, {k_arr, v_arr}) ->
          case Map.get(upd_record, k) do
            _ when k == :id -> {k_arr, v_arr}
            nil -> {k_arr, v_arr}
            val -> {[k | k_arr], [transform_out(module, k, val) | v_arr]}
          end
        end)
        {upd_keys, upd_vals} =
          update_auto_fields(module.ignore_auto_fields, {keys, vals},
                             {[:updated_at], [conn_module.get_date_time_now]})
        opts = [where: [id: record.id |> make_sure_int]]
        {upd_keys, upd_vals, opts}
      end
      defp update_record(record) do
        module = record.__struct__
        {upd_keys, upd_vals, opts} = prep_update(record)
        conn_module.update(conn_pool, module.table, upd_keys, upd_vals, opts)
      end
      # Transaction Version
      defp update_record(record, conn) when is_pid(conn) do
        module = record.__struct__
        {upd_keys, upd_vals, opts} = prep_update(record)
        conn_module.update_trx(conn, module.table, upd_keys, upd_vals, opts)
      end

      defp update_records(records) when is_list(records) do
        record = records |> hd
        module = record.__struct__
        batch_list = Enum.map(records, &prep_update/1)
        conn_module.batch_update(conn_pool, module.table, batch_list)
      end

      # Transaction Version
      defp update_records(records, conn) when is_list(records) and is_pid(conn) do
        record = records |> hd
        module = record.__struct__
        batch_list = Enum.map(records, &prep_update/1)
        conn_module.batch_update_trx(conn, module.table, batch_list)
      end

      defp delete(module, where: where) when length(where) > 0 do
        opts = [where: where]
        conn_module.delete(conn_pool, module.table, opts)
      end

      defp transaction(code_block) do
        conn_module.transaction(conn_pool, code_block)
      end


      #--- DDL -----
      def ddl_create_table(module) do
        flds = module.keys |> Enum.map(&(get_fld_desc(module.field_spec(&1)))) |> Enum.join(",")
        ddl_sql = "create table #{module.table} ( #{flds}, PRIMARY KEY (`id`));"
        Logger.debug(ddl_sql)
        conn_module.raw_exec(conn_pool, ddl_sql)
      end
      defp get_fld_desc(fldspec) do
        case fldspec do
          [:id, :int] -> "id INT(11) unsigned NOT NULL AUTO_INCREMENT"
          [name, :str, size] -> "#{name} VARCHAR(#{size})"
          [name, :str] -> "#{name} VARCHAR(255)"
          [name, :int] -> "#{name} INT(11)"
          [name, :money] -> "#{name} DECIMAL(10,2)"
          [name, :flat, size] -> "#{name} FLOAT(#{size |> to_string |> String.replace(".",",")})"
          [name, :date] -> "#{name} DATE"
          [name, :date_time] -> "#{name} TIMESTAMP"
          [name, :bool] -> "#{name} TINYINT(1)"
        end
      end

      # <<< ------ Import CSV file -------
      defp load_csv_file(module, csv_file, skip_first \\ true) when is_atom(module) do
        {:ok, fh} = :file.open(csv_file, :read)
        if skip_first do
          :file.read_line(fh)
        end
        process_data(fh, :file.read_line(fh), fn(data) ->
          sdata = data |> to_string
                  |> String.replace_trailing("\n","")
                  |> String.replace(~r/\"(.+),(.*)\"/,"\"\\1@@\\2\"")
                  |> String.split(",")
                  |> Enum.map(&String.replace(&1,"@@",","))
                  |> Enum.map(&String.replace(&1, ~r/\".*\"/,"\\1"))
          if length(sdata) > 1 do
            {:ok, _} = create_record(module, sdata)
          else
            nil
          end
        end)
      end

      defp process_data(fh, {:ok, :eof}, _) do
        :file.close(fh)
      end

      defp process_data(fh, {:ok, data}, pfunc) do
        pfunc.(data)
        process_data(fh, :file.read_line(fh), pfunc)
      end

      defp process_data(_, error, _) do
        Logger.error "File read returned unexpected result = #{inspect error}"
      end
      # -------- Import CSV file --------- >>>
    end
  end
end
