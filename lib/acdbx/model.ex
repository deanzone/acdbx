defmodule Acdbx.Model do
    def get_fld(fl) do
      case fl do
        {_,_,[f|_]} -> f
        {f, _} -> f
      end
    end
    def get_val(fl) do
      case fl do
        {_,_,v = [_|_]} -> v
        _ -> fl |> Tuple.to_list
      end
    end
  defmacro table(content) do
    quote do: def table, do: unquote(content)
  end

  defmacro relationships(content) do
    for {name, data} <- content do
      module = [Macro.to_string(data) |> String.split(",") |> Enum.fetch!(2) |> String.trim] |> Module.concat
      case Code.ensure_compiled(module) do
        {:error, _} -> raise "Module #{module} defined in relationship #{Macro.to_string(data)} does not exist"
        _ -> nil
      end
      quote do: def relationship(unquote(name)), do: unquote(data)
    end
  end
  defmacro fields(content) do
    flds = Enum.map(content, &get_fld/1)

    quote do
      defstruct(unquote(flds))
      unquote do
        for fld <- content do
          f1 = get_fld(fld)
          quote do: def field_spec(unquote(f1)), do: unquote(get_val(fld))
        end
      end
      def field_spec(name), do: raise "Missing Field Definition for '#{name}'. All relationship key fields must have a field defintion."
      unquote do
        quote do: def keys, do: unquote(flds |>Enum.filter(&!String.starts_with?(to_string(&1), "_")))
      end
    end
  end
  defmacro __using__(_options) do
    quote do
      import unquote(__MODULE__)
      #@spec get_model(String.t)::Map.t
      def get_model, do: Acdbx.Model.model
      def validate(_), do: {true, nil}
      def ignore_auto_fields, do: false
      defoverridable [ignore_auto_fields: 0]
    end
  end
  defmacro model do
    quote do: struct(__MODULE__)
  end
end
