defmodule Acdbx.MysqlConn do
  #defmacro __using__(_opts) do
  #  quote do
      use GenServer
      require Logger

      @spec server_time_zone :: String.t

      @replacers %{:nil => :null}

      # defp cfg, do: Application.get_env(:acdbx, unquote options[:config])
      # def pool_name, do: cfg[:pool_name]

      def server_time_zone, do: Application.get_env(:acdbx, :server_tz)
      def init(_opts = [config: cfg]) do
        Logger.info fn -> "Starting Mysql Connection to #{cfg[:db]}@#{cfg[:host]}..." end
        passwd = get_cfg_var(cfg, :passwd)
        user = get_cfg_var(cfg, :user)
        {:ok, conn} = :mysql.start_link(user: user, password: passwd,
                                               host: stlc(cfg[:host]), database: stlc(cfg[:db]),
                                               query_timeout: cfg[:timeout] || 5000,
                                               keep_alive: true)
        # :mysql.query(conn, "select 1")
        {:ok, conn}
      end

      def start_link(opts) do
        GenServer.start_link(__MODULE__, opts, [])
      end

      # === Client Thread =====

      defp sanitize(values) do
        values
        |> Enum.map(&Map.get(@replacers, &1, &1))
        |> Enum.map(&transform_v(&1))
      end

      defp is_debug, do: Application.get_env(:acdbx, :debug) || false

      defp debug(lfunc) do
        case is_debug do
          true -> Logger.debug lfunc.()
          false -> nil
        end
      end

      def test_transform(v), do: transform_v(v)

      #====== Private Funcs ======
      defp get_cfg_var(cfg, key) when is_atom(key) do
        case String.contains?(cfg[key], "%") do
          true -> String.replace(cfg[key], "%", "") |> :os.getenv()
          false -> stlc(cfg[key])
        end
      end

      defp sql_value(v) when is_binary(v), do: "'#{v}'"
      defp sql_value(v), do: v

      defp clean_special(s), do: s |> String.replace(~r/[\t\n]|\s+/," ")
      defp transform_v(v = {{_,_,_},{_,_,_}}), do: get_mysql_date_time(v)
      defp transform_v(v = {_,_,_}), do: get_mysql_date(v)
      defp transform_v(v=%{:__struct__ => Date}), do: get_mysql_date_time(v)
      defp transform_v(v=%{:__struct__ => DateTime}), do: get_mysql_date_time(v)
      defp transform_v(nil), do: :null
      defp transform_v(false), do: 0
      defp transform_v(true), do: 1
      defp transform_v(v), do: v

      defoverridable [transform_v: 1]

      defp get_result({{:ok, result},{:ok, max_rows}}), do: {result, max_rows}
      defp get_result({:ok, result}), do: result
      defp get_result(_), do: []

      #====== Server Thread =======
      # this is dangerous!
      # first you have to checkout the worker
      # then call this,
      # work with the connection
      # then check in the worker.
      def handle_call(:get_conn, _, conn) do
        {:reply, conn, conn}
      end
      def handle_call({:run_query, query, values, timeout}, _, conn) do
        debug(fn -> ":query - #{inspect query} #{inspect values} #{timeout}" end)

        {:reply,
         case :mysql.query(conn, query, values, timeout) do
           {:ok, columns, rows} -> col_atoms = Enum.map(columns, &(String.to_atom(&1)))
                                   {:ok, Enum.map(rows, &Enum.zip(col_atoms, &1) |> Enum.into(%{}))}
           {:error, {code1, code2, msg}}
                                -> Logger.error("Database Error: #{code1} - #{code2}, #{msg}")
                                   {:error, []}

           _                    -> {:notfound, []}
         end, conn}

      end

      def handle_call({:insert, prep_stmt, values}, _, conn) do
        debug(fn -> ":insert - #{inspect prep_stmt} #{inspect values}" end)
        {:reply,
         case :mysql.query(conn, prep_stmt, values) do
                                   :ok -> {:ok, :mysql.insert_id(conn)}
           {:error, {number, message}} -> {:error, "#{number} - #{message}"}
                                   msg -> {:error, "#{inspect msg}"}
         end, conn}
      end
      def handle_call({:update, prep_stmt, values}, _, conn) do
        debug(fn -> ":update - #{inspect prep_stmt} #{inspect values}" end)
        {:reply,
         case :mysql.query(conn, prep_stmt, values) do
                                   :ok -> {:ok, :mysql.affected_rows(conn)}
           {:error, {number, message}} -> {:error, "#{number} - #{message}"}
                                   msg -> {:error, "#{inspect msg}"}
         end, conn}
      end
      def handle_call({:batch_update, stmts}, _, conn) do
        debug(fn -> ":batch_update - #{inspect stmts}" end)
        {:reply,
         case :mysql.query(conn, stmts) do
                                   :ok -> {:ok, :mysql.affected_rows(conn)}
           {:error, {number, message}} -> {:error, "#{number} - #{message}"}
                                   msg -> {:error, "#{inspect msg}"}
         end, conn}
      end

      def handle_call({:exec, prep_stmt}, _, conn) do
        debug(fn -> ":exec - #{inspect prep_stmt}" end)
        {:reply, :mysql.query(conn, prep_stmt, []), conn}
      end
      def get_mysql_date(_erl_date = {y, m, d}), do: get_mysql_date({{y, m, d},nil})

      def get_mysql_date(erl_date = {{_y, _m, _d},_}) do
        Timex.to_datetime(erl_date, get_server_time_zone) |> Timex.format!("%F", :strftime)
      end
      def get_mysql_date_time(erl_date = {{_, _, _}, {_,_,_}}) do
        Timex.to_datetime(erl_date, get_server_time_zone) |> get_mysql_date_time
      end
      def get_mysql_date_time(date_time = %{:__struct__ => DateTime}) do
        Timex.format!(date_time, "%F %T", :strftime)
      end
      def get_mysql_date_time(date = %{:__struct__ => Date}) do
        Timex.format!(date, "%F", :strftime)
      end

      def get_server_time_zone, do: server_time_zone || "America/Chicago"
      def iso_to_mysql_date(date) do
        date
        |> Timex.parse!("{ISO:Extended}")
        |> Timex.Timezone.convert(get_server_time_zone)
        |> get_mysql_date_time

      end

      def get_today, do: Timex.now(get_server_time_zone) |> Timex.beginning_of_day |> get_mysql_date_time
      def get_from_today(days) do
        Timex.now(get_server_time_zone)
        |> Timex.beginning_of_day
        |> Timex.shift(days: days)
        |> get_mysql_date_time
      end
      def get_date_time_now do
        Timex.now(get_server_time_zone)
      end

      defp stlc(s), do: String.to_char_list(s)
      # ---- light ORM stuff -----
      #
      #
      defp decode_criteria({:or, fld_vals}) do
        {flds, vals} = Enum.map(fld_vals, &decode_criteria(&1))
        {"(#{Enum.join(flds, " or ")})", vals}
      end
      defp decode_criteria(fldval) do
        case fldval do
          {fld, [{:lt, val}]} -> {"#{fld} < ?", transform_v(val)}
          {fld, [{:lte, val}]} -> {"#{fld} <= ?", transform_v(val)}
          {fld, [{:gt, val}]} -> {"#{fld} > ?", transform_v(val)}
          {fld, [{:gte, val}]} -> {"#{fld} >= ?", transform_v(val)}
          {fld, [{:like, val}]} -> {"#{fld} like ?", transform_v(val)}
          {fld, [{:in, val}]} when is_list(val) -> {"#{fld} in (#{Enum.join(val, ",")})", nil}
          {fld, [{:in, val}]} -> {"#{fld} in (#{val})", nil}
          {fld, [{:within, {v1, v2}}]} -> {"(#{fld} >= ? and #{fld} <= ?)", [transform_v(v2), transform_v(v1)]} # v2, v1 due to reverse
          {fld, nil} -> {"isnull(#{fld})", nil}
          {fld, :today} -> {"(#{fld} > '#{get_from_today(-1)}' and #{fld} < '#{get_from_today(1)}')", nil}
          {fld, :null_date} -> {"#{fld}='0000-00-00'", nil}
          {fld, :some_date} -> {"(isnull(#{fld})=false and #{fld}<>'0000-00-00')", nil}
          {fld, :not_null} -> {"isnull(#{fld})=false", nil}
          {fld, :is_null} -> {"isnull(#{fld})", nil}
          {fld, val} -> {"#{fld}=?", transform_v(val)}
        end
      end
      defp extract_criteria(where) do
        {c, v} = Enum.reduce(where, {"1=1",[]},
                             fn(fldval,{acc,v}) ->
                               debug(fn -> inspect(fldval) end)
                                {_fld, _val} = case decode_criteria(fldval) do
                                  {fld, val} when is_list(val) ->
                                              {acc <> " and #{fld}", val ++ v}
                                  {fld, val} ->
                                              {acc <> " and #{fld}", [val | v]}
                                end
                             end)
        {c, Enum.reverse(v)}
      end
      defp extract_opts(opts) do
        select = case opts[:select] do
          nil -> "*"
          :all -> "*"
          fields when is_list(fields) -> Enum.join(fields, ",")
          _ -> "*"
        end
        {criteria, vals} = case opts[:where] do
          nil ->  []
          fields = [{_fld, _match} | _] -> fields
          fields = [{_fld, _term, _match} | _] -> fields
          _ -> []
        end
        |> extract_criteria
        order_by = case opts[:order_by] do
          nil -> ""
          {x, y} when y == :asc or y == :desc -> "order by #{x} #{y}"
          fields = [{_x,_y}|_] -> Enum.reduce(fields, [], fn({fld, ord}, acc) -> ["#{fld} #{ord}" | acc] end)
                                |> (fn(res) -> "order by " <> Enum.join(res, ", ") end).()
                         fld ->  "order by #{fld}"
        end
        group_by = case opts[:group_by] do
          nil -> ""
          flds when is_list(flds) -> "group by #{Enum.join(flds, ",")}"
          fld -> "group by #{fld}"
        end
        page = case opts[:page] do
          nil -> "LIMIT 1000"
          :nolimit -> ""
          {start, :nolimit} -> "OFFSET #{start}"
          {start, limit} -> "LIMIT #{limit} OFFSET #{start}"
          limit -> "LIMIT #{limit}"
        end

        [select: select, where: {criteria, Enum.filter(vals, &(&1 != nil))}, group_by: group_by, order_by: order_by, page: page]
      end

      def from(pool_name, {table1, table2}, opts) do
        joined_on = case opts[:on] do
          {j1, j2} -> "#{j1} = #{j2}"
          o = [_|_] -> Enum.map(o, fn({j1, j2}) -> "#{j1} = #{j2}" end)
                       |> Enum.join(" and ")
        end
        e_opts = extract_opts(opts)
        {criteria, vals} = e_opts[:where]
        sql = """
          select #{e_opts[:select]} from #{table1} t1 join #{table2} t2 on #{joined_on} where #{criteria} #{e_opts[:group_by]} #{e_opts[:order_by]}
          #{e_opts[:page]}
         """
        call_get_data(pool_name, sql, vals, opts[:timeout] || 5000)
        |> get_result
      end

      def from(pool_name, table, opts) when is_atom(table) do
        e_opts = extract_opts(opts)
        {criteria, vals} = e_opts[:where]

        sql ="select SQL_CALC_FOUND_ROWS #{e_opts[:select]} from #{table} where #{criteria} #{e_opts[:order_by]} #{e_opts[:group_by]} #{e_opts[:page]}"
        call_get_data(pool_name, sql, vals, opts[:timeout] || 1000)
        |> get_result

      end

      def raw_query(pool_name, sql, vals, timeout \\ 1000) do
        case String.starts_with?(sql, "update") || String.contains?(sql, "UPDATE") do
          true -> raise "call raw_update instead of raw_query"
          false -> call_get_data(pool_name, sql, vals, timeout + 5000)
                   |> get_result
        end
      end

      def raw_update(pool_name, sql, vals) do
          call_update(pool_name, sql, vals)
          |> get_result
      end
      def raw_exec(pool_name, sql) do
        :poolboy.transaction(pool_name, fn(worker) ->
          GenServer.call(worker, {:exec, sql |> clean_special})
        end)
      end

      defp get_max_rows(conn_pid) do
        case GenServer.call(conn_pid, {:run_query, "select FOUND_ROWS() max_rows", [], 1000}, 5000) do
          {:ok, [%{:max_rows => max_rows}|_ ]} -> {:ok, max_rows}
           _output -> {:ok, nil}
        end
      end

      defp call_get_data(pool_name, query, values, timeout \\ 1000) do
        sanitized = sanitize(values)
        :poolboy.transaction(pool_name, fn(conn_pid) ->
          result = GenServer.call(conn_pid, {:run_query, query |> clean_special, sanitized, timeout}, timeout + 5000)
          case result do
            {:ok, [%{:last_insert_id => _} | _]} -> result
                                               _ -> {result, get_max_rows(conn_pid)}
          end
        end)
      end

      defp call_update(pool_name, prep_stmt, values) do
        sanitized = values |> sanitize
        :poolboy.transaction(pool_name, fn(worker) ->
          GenServer.call(worker, {:update, prep_stmt |> clean_special, sanitized})
        end)
      end

      defp get_last_insert_ids(pool_name) do
        result = call_get_data(pool_name, "SELECT LAST_INSERT_ID() last_insert_id",[])
        debug(fn -> inspect result end)
        result |> get_result
        |> Enum.map(&Map.get(&1, :last_insert_id))
      end

      def insert(pool_name, table, keys, values) when is_list(keys) and is_list(values) do
        key_str = keys |> Enum.map(fn(v) -> "`#{v}`" end) |> Enum.join(",")
        templ = keys |> Enum.map(fn(_) -> "?" end) |> Enum.join(",")
        sql = "insert into #{table} (#{key_str}) values(#{templ})"
        upd_values = Enum.map(values, &transform_v(&1))
        debug(fn -> "#{sql} - #{inspect upd_values}" end)

        :poolboy.transaction(pool_name, fn(worker) ->
          GenServer.call(worker, {:insert, sql |> clean_special, upd_values |> sanitize})
        end)

      end

      def batch_insert(pool_name, table, insert_list) when is_list(insert_list) do
        batch_sql =
          for {keys, values} <- insert_list do
            key_str = keys |> Enum.map(fn(v) -> "`#{v}`" end) |> Enum.join(",")
            value_str = Enum.map(values, &sql_value(transform_v(&1))) |> Enum.join(",")
            "insert into #{table} (#{key_str}) values(#{value_str})"
          end
          |> Enum.join(";")
          debug(fn -> batch_sql <> ";" end)

        result =
          :poolboy.transaction(pool_name, fn(worker) ->
            GenServer.call(worker, {:batch_update, batch_sql |> clean_special})
          end)
        case result do
          {:ok, affected_rows} when affected_rows > 0 ->
                        {:ok, affected_rows, get_last_insert_ids(pool_name)}
          error                 ->
                        error
        end
      end

      def upsert(pool_name, table, keys, values) when is_list(keys) and is_list(values) do
        key_str = keys |> Enum.map(fn(v) -> "`#{v}`" end) |> Enum.join(",")
        templ = keys |> Enum.map(fn(_) -> "?" end) |> Enum.join(",")
        sql = """
          insert into #{table} (#{key_str}) values(#{templ}) on duplicate key
          update updated_at = current_timestamp()
          """
        upd_values = Enum.map(values, &transform_v(&1))
        debug(fn -> "upsert - #{sql} - #{inspect upd_values}" end)

        :poolboy.transaction(pool_name, fn(worker) ->
          GenServer.call(worker, {:insert, sql |> clean_special, upd_values |> sanitize})
        end)
      end

      def update(pool_name, table, keys, values, opts) when is_list(keys) and is_list(values) do
        key_str = keys |> Enum.map(fn(v) -> "`#{v}` = ?" end) |> Enum.join(",")
        {where, w_vals} = extract_criteria(opts[:where])
        upd_values = Enum.map(values ++ w_vals, &transform_v(&1))

        sql = "update #{table} set #{key_str} where #{where}"
        debug(fn -> "#{sql} - #{inspect upd_values}" end)
        call_update(pool_name, sql, upd_values)
      end

      defp batch_update_prep(table, update_list) when is_list(update_list) do
       for {keys, values, opts} <- update_list do
          where = opts[:where]
          upd_str = Enum.zip(keys, values)
                    |> Enum.map(fn({k,v}) -> "`#{k}` = #{sql_value(transform_v(v))}" end)
                    |> Enum.join(",")
          "update #{table} set #{upd_str} where id = #{where[:id]}"
        end
        |> Enum.join(";")
      end
      def batch_update(pool_name, table, update_list) when is_list(update_list) do
        batch_sql = batch_update_prep(table, update_list)
        debug(fn -> batch_sql <> ";" end)

        :poolboy.transaction(pool_name, fn(worker) ->
          GenServer.call(worker, {:batch_update, batch_sql |> clean_special})
        end)
      end

      def delete(pool_name, table, opts) when is_list(opts) do
        {where, w_vals} = extract_criteria(opts[:where])
        upd_values = Enum.map(w_vals, &transform_v(&1))

        sql = "delete from #{table} where #{where}"
        debug(fn -> "#{sql} - #{inspect upd_values}" end)
        call_update(pool_name, sql, upd_values)
      end

      # <<<------------- Transaction Handling --------------------
      # used by repo for transactions
      def transaction(pool_name, execute_block) do
        worker = :poolboy.checkout(pool_name)
        try do
          conn = GenServer.call(worker, :get_conn)
          :mysql.transaction(conn, execute_block, [conn], 1)
        after
          :poolboy.checkin(pool_name, worker)
        end
      end

      def insert_trx(conn, table, keys, values) when is_list(keys) and is_list(values) do
        key_str = keys |> Enum.map(fn(v) -> "`#{v}`" end) |> Enum.join(",")
        templ = keys |> Enum.map(fn(_) -> "?" end) |> Enum.join(",")
        sql = "insert into #{table} (#{key_str}) values(#{templ})"
        upd_values = Enum.map(values, &transform_v(&1))
        debug(fn -> "in_transaction => #{sql} - #{inspect upd_values}" end)

        case :mysql.query(conn, sql |> clean_special, upd_values |> sanitize) do
                                   :ok -> {:ok, :mysql.insert_id(conn)}
           {:error, {number, message}} -> {:error, "#{number} - #{message}"}
                                   msg -> {:error, "#{inspect msg}"}
         end
      end

      def update_trx(conn, table, keys, values, opts) when is_list(keys) and is_list(values) do
        key_str = keys |> Enum.map(fn(v) -> "`#{v}` = ?" end) |> Enum.join(",")
        {where, w_vals} = extract_criteria(opts[:where])
        upd_values = Enum.map(values ++ w_vals, &transform_v(&1))

        sql = "update #{table} set #{key_str} where #{where}"
        debug(fn -> "in_transaction => #{sql} - #{inspect upd_values}" end)
        case :mysql.query(conn, sql |> clean_special, upd_values |> sanitize) do
                                   :ok -> {:ok, :mysql.affected_rows(conn)}
           {:error, {number, message}} -> {:error, "#{number} - #{message}"}
                                   msg -> {:error, "#{inspect msg}"}
         end
      end

      def batch_update_trx(conn, table, update_list) when is_list(update_list) do
        batch_sql = batch_update_prep(table, update_list)
        debug(fn -> "trx ==> " <> batch_sql <> ";" end)

        case :mysql.query(conn, batch_sql) do
                                   :ok -> {:ok, :mysql.affected_rows(conn)}
           {:error, {number, message}} -> {:error, "#{number} - #{message}"}
                                   msg -> {:error, "#{inspect msg}"}
         end
      end

      # ------------- Transaction Handling ------------>>>

      # end
    #  end
end

#defmodule Acdbx.MysqlConn do
#  use Acdbx.MysqlConnBase
#  def server_time_zone, do: application.get_env(:acdbx, :server_tz)
#end
