defmodule Decimal do
  defstruct [num: 0, dec: 0]

  def max_prec, do: 2

  defp get_signed(n, n2) when n < 0, do: 0 - n2
  defp get_signed(_n, n2), do: n2

  defp str_to_integer(num, prec) do
    #IO.puts "#{num}, #{prec}"
    {n, s} = Integer.parse(:io_lib.format("~-35.#{prec}f",[num * 1.0]) |> to_string |> String.trim)
    s = String.slice(s, 1..10)
    nprec = min(String.length(s), max_prec)
    np = (abs(n) * :math.pow(10, nprec)) + abs(String.to_integer(s))
    {get_signed(num, round(np)), nprec}
  end

  def new(num),  do: new(num, max_prec)
  def new(num, prec) do
    case num do
      _ when is_integer(num) -> %Decimal{num: num, dec: prec}
      _ when is_float(num) ->
        {n, p} = str_to_integer(num, prec)
        %Decimal{num: n, dec: p}
      _ when is_binary(num) ->
        case Regex.run(~r/[0-9.]+/, num) do
          nil -> raise "#{inspect num} is not a valid number"
          [n1] -> Float.parse(n1) |> elem(0) |> Decimal.new(prec)
        end
      _ -> raise "#{inspect num} is not a valid number"
    end
  end

  #def get_float(num, prec) do
    #String.pad_trailing("#{num}", prec, "0")
    #|> String.to_integer
  #end


  #def equalize1(d1, d2) do
    #case d1.dec > d2.dec do
      #true -> {d1.num, get_float(d2.num, d1.dec - d2.dec), d1.dec}
      #false -> {get_float(d1.num , d2.dec - d1.dec), d2.num, d2.dec}
    #end
  #end

  # take two floats and make them the same precision by promoting them
  # to integer, then extract the precision
  defp equalize(d1, d2) do
    case d1.dec > d2.dec do
      true -> {d1.num, round(d2.num * :math.pow(10, d1.dec - d2.dec)), d1.dec}
      false -> {round(d1.num * :math.pow(10, d2.dec - d1.dec)), d2.num, d2.dec}
    end
  end
  def add(d1, d2) do
    {d1p, d2p, np} = equalize(d1, d2)
    new(d1p + d2p, np)
  end

  def sub(d1, d2) do
    {d1p, d2p, np} = equalize(d1, d2)
    new(d1p - d2p, np)
  end

  def mul(d1, d2) do
    {d1p, d2p, np} = equalize(d1, d2)
    new(d1p * d2p, np + np)
  end

  def div(d1, d2) do
    {d1p, d2p, np} = equalize(d1, d2)
    new(d1p / d2p, np)
  end

  defp get_padded(n, d) do
    to_string(n) |> String.pad_leading(d + 1, "0")
  end

  def as_string(d) do
    {sign, num_str} =
      case d.num > 0 do
        true -> {"", get_padded(d.num, d.dec)}
        false -> {"-", get_padded(abs(d.num), d.dec)}
      end

    {d1, d2} = String.split_at(num_str, 0 - d.dec)
    "#{sign}#{d1}.#{d2}"
  end

  def as_float(d) do
    as_string(d) |> Float.parse |> elem(0)
  end
  #def get_float(d), do: round(d.num) / :math.pow(10.0,prec)


  #------- Tests -------
  #defp gen_num() do
    #nums = 0..10000
    #"#{Enum.random(nums)}.#{Enum.random(nums)}"
  #end

  #defp f_round(amt, prec), do: round(amt * :math.pow(10,prec)) / :math.pow(10.0,prec)

  #defp run_test(count, mfunc) do
    #IO.puts " Running test #{mfunc}"

    #1..count |> Enum.each(fn(_) ->
      #d1 = gen_num()
      #d2 = gen_num()
      #{f1, _} = Float.parse(d1)
      #{f2, _} = Float.parse(d2)
      #{d3, d4} = :erlang.apply(Decimal, mfunc, [d1, d2, f1, f2])
      #if f_round(d4, max_prec) > (1 / :math.pow(10, max_prec)) do
        #IO.puts "#{d3}, #{f1}, #{f2}, #{d4} - no match"
      #end

    #end)
  #end
  #defp add_test(d1, d2, f1, f2) do
    #d3 = Decimal.add(Decimal.new(d1), Decimal.new(d2)) |> Decimal.get_float
    #d4 = f_round(abs(d3 - (f1 + f2)), 5)
    #{d3, d4}
  #end

  #defp sub_test(d1, d2, f1, f2) do
    #d3 = Decimal.sub(Decimal.new(d1), Decimal.new(d2)) |> Decimal.get_float
    #d4 = f_round(abs(d3 - (f1 - f2)), 5)
    #{d3, d4}
  #end
  #defp mul_test(d1, d2, f1, f2) do
    #d3 = Decimal.mul(Decimal.new(d1), Decimal.new(d2)) |> Decimal.get_float
    #d4 = f_round(abs(d3 - (f1 * f2)), 5)
    #{d3, d4}
  #end
  #defp div_test(d1, d2, f1, f2) do
    #d3 = Decimal.div(Decimal.new(d1), Decimal.new(d2)) |> Decimal.get_float
    #d4 = f_round(abs(d3 - (f1 / f2)), 5)
    #{d3, d4}
  #end

  #def test_all(count) do
    #run_test(count, :add_test)
    #run_test(count, :sub_test)
    #run_test(count, :mul_test)
    #run_test(count, :div_test)
  #end

end
