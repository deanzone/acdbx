defmodule Example.Repo do
  use Acdbx.Repo, conn: :core_db
  alias Example.Models

  def get_customers_with_location(count) do
      # get a customers with primary_address and locations relationships.
      # if the query exceeds 100 records, it will take first 100 records before filling the relationships.
      get_with(Models.Customer,[:primary_address, :locations], page: count)
      # for each customer retrieve location address
      |> Enum.map(fn(customer) -> %{customer | :locations => get_rel(customer.locations, :address)} end)
  end

  def get_customer_with_location(id) do
    # get a customer with primary_address and locations relationships
    customer = get_one_with(Models.Customer, id, [:primary_address, :locations])
    # now fill address for locations
    %{customer | :locations => get_rel(customer.locations, :address)}
  end

  def get_customers_in_int(ids) when is_list(ids) do
    get_records(Models.Customer, where: [id: [in: ids]])
  end

  def get_customers_in_str(customer_codes) when is_list(customer_codes) do
    get_records(Models.Customer, where: [pbx_context: [in: customer_codes]])
  end

  def get_invoices(start, limit)  do
    get_records(Models.Invoice, page: {start,limit})
  end

  def get_timestamp do
    sql = "select current_timestamp"
    raw_query(sql, [])
  end

  def save_invoice(inv) do
    update_record(inv)
  end

  def test_transaction do
    customer = get_record(Models.Customer, 4)
    transaction(fn(conn) ->
      {:ok, address_id} = %Models.Address{
        addressee: "Mike Smith",
        address1: "505 Legacy Drive",
        city: "Plano",
        postal_code: "75074",
        state: "TX"
      } |> create_record(conn)
      IO.puts ("Address Id #{address_id}")
      throw "blah blah"
      %{customer | primary_address_id: address_id} |> update_record(conn)
    end)
  end
end
