defmodule Example.Models.Customer do
  use Acdbx.Model

  table :customers

  fields [
      {:id, :int},
      {:company_name, :str, 50},
      {:billing_date, :int},
      {:pbx_context, :str},
      {:customer_config_id, :int},
      {:created_at, :date_time},
      {:primary_address_id, :int}
    ]

  # name: {<[:one_to_one, :one_to_many]>, :source_id, :Target_table <[, :target_id]>}}
    relationships [
      address: {:one_to_one, :primary_address_id, Example.Models.Address, :id},
      config: {:one_to_one, :customer_config_id, Example.Models.CustomerConfig, :id}
    ]
end
