defmodule Example.Models.CustomerConfig do
  use Acdbx.Model

  table :customer_config

  fields [
    {:id, :int},
    {:voice_on_demand_paths, :int},
    {:us_ca_per_minute_rate, :money},
    {:incoming_toll_free_per_minute_rate, :money},
    {:conference_bridge_per_minute_rate, :money},
    {:created_at, :date_time},
    {:updated_at, :date_time},
    {:call_recording, :int, 1},
    {:call_center, :int, 1},
    {:feature_server_id, :int},
    {:available_extensions, :int},
    {:available_cloud_extensions, :int},
    {:available_paths, :int},
    {:available_agents, :int},
    {:forced_carrier_id, :int}
  ]

#  relationships [
#    name: {<|:one_to_one | :one_to_many |>, :src_id, Model, :assoc_id}
#  ]
end
