defmodule Example.Models.Address do
  use Acdbx.Model

  table :addresses

  fields [
     {:id, :int},
     {:addressee, :str, 30},
     {:address1, :str, 30},
     {:address2, :str, 30},
     {:city, :str, 30},
     {:state, :str, 2},
     {:postal_code, :str, 9},
     {:created_at, :date_time},
     {:updated_at, :date_time}
   ]

  # name: {<[:one_to_one, :one_to_many]>, :source_id, :target_table <[, :target_id]>}}
  # relationships []
end
